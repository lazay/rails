class AddPublishedToSlide < ActiveRecord::Migration[5.0]
  def change
    add_column :slides, :published, :boolean
    add_column :slides, :published_to, :datetime
    add_column :slides, :published_from, :datetime
  end
end
